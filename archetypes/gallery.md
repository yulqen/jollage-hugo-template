---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
weight: 10
draft: false
tags: ['untagged']
image: "img/FILE_NAME_HERE"
image_alt: "SHORT DESCRIPTION OF IMAGE"
description: "DESCRIPTION"
filename: "{{ .File.BaseFileName }}"
---

